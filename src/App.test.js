import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import Amplify, { Analytics } from 'aws-amplify';
import aws_exports from './aws-exports';

Amplify.configure(aws_exports);



render(){
  return(
    <>
      <h1>Vote Your Language!</h1>
      <Sidebar items={items} />
      <div className="languages">
      
        {
          this.state.languages.map((lang, i) => 
            <div key={i} className="language">
              <div className="voteCount">
                {lang.votes}
              </div>
              <div className="languageName">
                {lang.name}
              </div>
              <button onClick={this.vote.bind(this, i)}>Click Here</button>
            </div>
          )
        }
      </div>
    </>
  );
}
